<%@page import="com.crmmvc.util.PathConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="dec"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="assets/plugins/images/favicon.png">
<title><dec:title default="CRM APP" /></title>
<dec:head />
<jsp:include page="<%=PathConstants.PATH_STYLE%>" />
</head>

<body>
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">
		<!-- Navigation -->
		<jsp:include page="<%=PathConstants.PATH_NAVBAR%>" />
		<!-- Left navbar-header -->
		<jsp:include page="<%=PathConstants.PATH_SIDEBAR%>" />
		<!-- Left navbar-header end -->
		<!-- Page Content -->
		<div id="page-wrapper">
			<dec:body />
		</div>
		<!-- /.container-fluid -->
	</div>
	<jsp:include page="<%=PathConstants.PATH_FOOTER%>" />
	<!-- /#page-wrapper -->
	<!-- /#wrapper -->
	<jsp:include page="<%=PathConstants.PATH_SCRIPT%>" />
</body>

</html>