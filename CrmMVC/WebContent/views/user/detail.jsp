<%@page import="com.crmmvc.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<title>USER DETAIL</title>
</head>
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Chi tiết thành viên</h4>
		</div>
	</div>
	<!-- /.row -->
	<!-- .row -->
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="white-box">
				<div class="user-bg">
					<img width="100%" alt="user"
						src="assets/plugins/images/large/img1.jpg">
					<div class="overlay-box">
						<div class="user-content">
							<a href="javascript:void(0)"><img
								src="assets/plugins/images/users/genu.jpg"
								class="thumb-lg img-circle" alt="img"></a>
							<h4 class="text-white">${item.fullname }</h4>
							<h5 class="text-white">${item.email }</h5>
						</div>
					</div>
				</div>
				<div class="user-btm-box">
					<div class="col-md-4 col-sm-4 text-center">
						<p class="text-purple">
							<i class="ti-facebook"></i>
						</p>
						<h4>${pt.notDone }%</h4>
						<h6>Chưa thực hiện</h6>
					</div>
					<div class="col-md-4 col-sm-4 text-center">
						<p class="text-blue">
							<i class="ti-twitter"></i>
						</p>
						<h4>${pt.inProgress }%</h4>
						<h6>Đang thực hiện</h6>
					</div>
					<div class="col-md-4 col-sm-4 text-center">
						<p class="text-danger">
							<i class="ti-dribbble"></i>
						</p>
						<h4>${pt.done }%</h4>
						<h6>Hoàn thành</h6>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8 col-xs-12">
			<div class="white-box">
				<form class="form-horizontal form-material">
					<div class="form-group">
						<label class="col-md-12">Full Name</label>
						<div class="col-md-12">
							<p>${item.fullname }</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Email</label>
						<div class="col-md-12">
							<p>${item.email }</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Password</label>
						<div class="col-md-12">
							<p>${item.password }</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-12">Select Country</label>
						<div class="col-md-12">
							<p>${item.role_name }</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br />
	<!-- /.row -->
	<!-- BEGIN DANH SÁCH CÔNG VIỆC -->
	<h4>DANH SÁCH CÔNG VIỆC</h4>
	<div class="row">
		<c:forEach var="item" items="${listTasksByStatus }">
			<div class="col-xs-12">
				<h5>Tình trạng: ${item.status_name }</h5>
				<c:forEach var="taskDTO" items="${item.listTaskDTO }">
					<div class="col-md-4">
						<div class="white-box">
							<a
								href='<c:url value="<%=UrlConstants.URL_TASK_DETAIL %>"/>?id=${taskDTO.task_id}'>
								<h3 class="box-title">Tên công việc: ${taskDTO.job_name }</h3>
								<h3 class="box-title">Tên Task: ${taskDTO.task_name }</h3>
								<div class="message-center">
									<div class="mail-contnet">
										<p class="mail-desc">
											Ngày bắt đầu: <span class="time">${taskDTO.start_date }</span>
										</p>
										<p class="mail-desc">
											Ngày kết thúc: <span class="time">${taskDTO.end_date }</span>
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</c:forEach>
			</div>
		</c:forEach>
	</div>
	<!-- END DANH SÁCH CÔNG VIỆC -->
</div>