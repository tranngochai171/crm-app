<%@page import="com.crmmvc.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<title>TASK DETAIL</title>
</head>
<body>
	<div class="container">
		<p>Tên công việc: ${item.job_name }</p>
		<p>Tên Task: ${item.task_name }</p>
		<p>Người thực hiện: ${item.user_fullname }</p>
		<p>Ngày bắt đầu: ${item.start_date }</p>
		<p>Ngày kết thúc: ${item.end_date }</p>
		<form class="form-horizontal form-material"
			action='<c:url value="<%=UrlConstants.URL_TASK_DETAIL%>"/>'
			method="POST">
			<input type="hidden" name="task_id" value=${item.task_id }
				class="form-control form-control-line" readonly="readonly">
			<input type="hidden" name="job_id" value=${item.job_id }
				class="form-control form-control-line" readonly="readonly">
			<div class="form-group">
				<label class="col-sm-12">Trạng thái</label>
				<div class="col-sm-12">
					<select class="form-control form-control-line" name="status_id">
						<c:forEach var="status" items="${listStatus }">
							<option
								<c:if test="${status.id == item.status_id }">selected</c:if>
								value="${status.id }">${status.name }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-success">Update
						Status</button>
				</div>
			</div>
		</form>
	</div>
</body>
