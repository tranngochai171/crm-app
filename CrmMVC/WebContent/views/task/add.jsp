<%@page import="com.crmmvc.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<title>TASK ADD</title>
</head>
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Thêm mới Task</h4>
			<c:if test="${message != null }">
				<p class="text-warning">${message }</p>
			</c:if>
		</div>
	</div>
	<!-- /.row -->
	<!-- .row -->
	<div class="row">
		<div class="col-md-2 col-12"></div>
		<div class="col-md-8 col-xs-12">
			<div class="white-box">
				<form class="form-horizontal form-material"
					action='<c:url value="<%=UrlConstants.URL_TASK_ADD%>"/>'
					method="POST">
					<input type="hidden" class="form-control form-control-line"
						name="job_id" value=${job.id } readonly="readonly">
					<div class="form-group">
						<label class="col-md-12">Tên công việc đã được chỉ định
							trong task</label>
						<div class="col-md-12">
							<input type="text" class="form-control form-control-line"
								value="${job.name }" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Tên Task</label>
						<div class="col-md-12">
							<input type="text" class="form-control form-control-line"
								name="name" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ngày bắt đầu</label>
						<div class="col-md-12">
							<input type="date" name="start_date"
								class="form-control form-control-line" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ngày kết thúc</label>
						<div class="col-md-12">
							<input type="date" name="end_date"
								class="form-control form-control-line" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-12">Người thực hiện</label>
						<div class="col-sm-12">
							<select class="form-control form-control-line" name="user_id">
								<c:forEach var="item" items="${listUser }">
									<option value="${item.id }">${item.fullname }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-12">Trạng thái</label>
						<div class="col-sm-12">
							<select class="form-control form-control-line" name="status_id">
								<c:forEach var="item" items="${listStatus }">
									<option value="${item.id }">${item.name }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-success">Add Task</button>
							<a href='<c:url value="<%=UrlConstants.URL_USER%>"/>'
								class="btn btn-primary">Quay lại</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-2 col-12"></div>
	</div>
	<!-- /.row -->
</div>