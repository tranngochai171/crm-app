<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<footer class="footer text-center"> 2018 &copy; myclass.com </footer>
<input type="hidden" id="successfulNotify" value="${success }" />
<input type="hidden" id="failedNotify" value="${fail }" />