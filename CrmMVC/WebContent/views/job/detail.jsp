<%@page import="com.crmmvc.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JOB DETAIL</title>
</head>
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Chi tiết công việc</h4>
		</div>
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
			<a
				href='<c:url value="<%=UrlConstants.URL_TASK_ADD%>"/>?job_id=${item.id}'
				class="btn btn-sm btn-success">Thêm mới Task</a>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- BEGIN THỐNG KÊ -->
	<div class="row">
		<!--col -->
		<div class="col-md-12 col-xs-12">
			<div class="white-box">
				<form class="form-horizontal form-material">
					<div class="form-group">
						<label class="col-md-12">Tên công việc</label>
						<div class="col-md-12">
							<p>${item.name }</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ngày bắt đầu</label>
						<div class="col-md-12">
							<p>${item.start_date }</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ngày kết thúc</label>
						<div class="col-md-12">
							<p>${item.end_date }</p>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="white-box">
				<div class="col-in row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<i data-icon="E" class="linea-icon linea-basic"></i>
						<h5 class="text-muted vb">CHƯA BẮT ĐẦU</h5>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<h3 class="counter text-right m-t-15 text-danger">${pt.notDone }%</h3>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar"
								aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
								style="width:${pt.notDone }%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.col -->
		<!--col -->
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="white-box">
				<div class="col-in row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<i class="linea-icon linea-basic" data-icon="&#xe01b;"></i>
						<h5 class="text-muted vb">ĐANG THỰC HIỆN</h5>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<h3 class="counter text-right m-t-15 text-megna">${pt.inProgress }%</h3>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
							<div class="progress-bar progress-bar-megna" role="progressbar"
								aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
								style="width: ${pt.inProgress }%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.col -->
		<!--col -->
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="white-box">
				<div class="col-in row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<i class="linea-icon linea-basic" data-icon="&#xe00b;"></i>
						<h5 class="text-muted vb">HOÀN THÀNH</h5>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<h3 class="counter text-right m-t-15 text-primary">${pt.done }%</h3>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
							<div class="progress-bar progress-bar-primary" role="progressbar"
								aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
								style="width: ${pt.done }%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- END THỐNG KÊ -->

	<!-- BEGIN DANH SÁCH CÔNG VIỆC -->
	<div class="row">
		<!-- <div class="col-xs-12">
			<a href="#" class="group-title"> <img width="30"
				src="assets/plugins/images/users/pawandeep.jpg" class="img-circle" />
				<span>Pavan kumar</span>
			</a>
		</div> -->
		<c:forEach var="item" items="${listTasksByStatus}">
			<div class="col-xs-12">
				<p>${item.status_name }</p>
				<c:forEach var="taskDTO" items="${item.listTaskDTO }">
					<div class="col-md-4">
						<div class="white-box">
							<div class="message-center">
								<a
									href='<c:url value="<%=UrlConstants.URL_TASK_DETAIL %>"/>?id=${taskDTO.task_id}'>
									<div class="mail-contnet">
										<h4>Tên task: ${taskDTO.task_name }</h4>
										<h5>Người thực hiện: ${taskDTO.user_fullname }</h5>
										<p class="mail-desc">
											Ngày bắt đầu:<span class="time">${taskDTO.start_date }</span>
										</p>
										<p class="mail-desc">
											Ngày kết thúc:<span class="time">${taskDTO.end_date }</span>
										</p>
									</div>
								</a>

							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</c:forEach>
	</div>
	<!-- END DANH SÁCH CÔNG VIỆC -->
</div>