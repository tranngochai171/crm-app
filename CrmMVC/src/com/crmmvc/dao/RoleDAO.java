package com.crmmvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.crmmvc.connection.JDBCConnection;
import com.crmmvc.model.Role;

public class RoleDAO {
	public List<Role> getListRole() {
		List<Role> listRole = new LinkedList<Role>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM roles;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Role role = new Role();
				role.setId(res.getInt("id"));
				role.setName(res.getString("name"));
				role.setDescription(res.getString("description"));
				listRole.add(role);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRole;
	}

	public int addNewRole(Role role) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO roles (name,description) VALUES (?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, role.getName());
			stm.setString(2, role.getDescription());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteRole(int id_delete) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM roles WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id_delete);
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Role findById(int id) {
		Role role = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM roles WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				role = new Role();
				role.setId(id);
				role.setName(res.getString("name"));
				role.setDescription(res.getString("description"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;
	}

	public int editRole(Role role) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE roles SET name = ?, description = ? WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, role.getName());
			stm.setString(2, role.getDescription());
			stm.setInt(3, role.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
