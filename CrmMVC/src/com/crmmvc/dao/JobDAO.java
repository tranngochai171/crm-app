package com.crmmvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.crmmvc.connection.JDBCConnection;
import com.crmmvc.model.Job;

public class JobDAO {
	public List<Job> getListJob() {
		List<Job> listJob = new LinkedList<Job>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM jobs;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Job job = new Job();
				job.setId(res.getInt("id"));
				job.setName(res.getString("name"));
				job.setStart_date(res.getDate("start_date"));
				job.setEnd_date(res.getDate("end_date"));
				listJob.add(job);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listJob;
	}

	public int addNewJob(Job job) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO jobs (name,start_date,end_date) VALUES (?,?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, job.getName());
			stm.setDate(2, job.getStart_date());
			stm.setDate(3, job.getEnd_date());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteJob(int id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM jobs WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Job findById(int id) {
		Job job = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM jobs WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				job = new Job();
				job.setId(res.getInt("id"));
				job.setName(res.getString("name"));
				job.setStart_date(res.getDate("start_date"));
				job.setEnd_date(res.getDate("end_date"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return job;
	}

	public int editJob(Job job) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE jobs SET name = ?, start_date = ?, end_date = ? WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, job.getName());
			stm.setDate(2, job.getStart_date());
			stm.setDate(3, job.getEnd_date());
			stm.setInt(4, job.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int[] getPercentageOfWords(int job_id) {
		int[] array = new int[4];
		final int NUMBER_OF_STATUS = 3;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT count(tasks.id) as numberOfTasks FROM tasks JOIN jobs "
					+ "ON tasks.job_id = jobs.id WHERE jobs.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, job_id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				array[0] = res.getInt("numberOfTasks");
			}
			for (int i = 1; i <= NUMBER_OF_STATUS; i++) {
				array[i] = getNumberOfTask(job_id, i);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}

	private int getNumberOfTask(int job_id, int status_id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT count(tasks.id) as numberOfTasks FROM tasks JOIN jobs ON tasks.job_id = jobs.id"
					+ " JOIN status on tasks.status_id = status.id" + " WHERE jobs.id = ? AND status.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, job_id);
			stm.setInt(2, status_id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				result = res.getInt("numberOfTasks");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
