package com.crmmvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.crmmvc.connection.JDBCConnection;
import com.crmmvc.dto.TaskDTO;
import com.crmmvc.model.Task;
import com.crmmvc.model.TasksByStatus;

public class TaskDAO {
	public List<Task> getListTask() {
		List<Task> listTask = new LinkedList<Task>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM tasks;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Task task = new Task();
				task.setId(res.getInt("id"));
				task.setName(res.getString("name"));
				task.setStart_date(res.getDate("start_date"));
				task.setEnd_date(res.getDate("end_date"));
				task.setUser_id(res.getInt("user_id"));
				task.setJob_id(res.getInt("job_id"));
				task.setStatus_id(res.getInt("status_id"));
				listTask.add(task);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listTask;
	}

	public TaskDTO getTaskDTOByTaskId(int task_id) {
		TaskDTO taskDTO = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT tasks.id as task_id, tasks.name as task_name, tasks.start_date, tasks.end_date,\n"
					+ "users.id as user_id, users.fullname as user_fullname, status.id as status_id ,status.name as status_name,\n"
					+ "jobs.name as job_name, jobs.id as job_id FROM tasks \n" + "JOIN jobs on tasks.job_id = jobs.id\n"
					+ "JOIN users on tasks.user_id = users.id\n"
					+ "JOIN status on tasks.status_id = status.id WHERE tasks.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, task_id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				taskDTO = new TaskDTO();
				taskDTO.setJob_id(res.getInt("job_id"));
				taskDTO.setJob_name(res.getString("job_name"));
				taskDTO.setTask_name(res.getString("task_name"));
				taskDTO.setStart_date(res.getDate("start_date"));
				taskDTO.setEnd_date(res.getDate("end_date"));
				taskDTO.setStatus_name(res.getString("status_name"));
				taskDTO.setUser_id(res.getInt("user_id"));
				taskDTO.setUser_fullname(res.getString("user_fullname"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return taskDTO;
	}

	public Task getById(int id) {
		Task task = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM tasks WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				task = new Task();
				task.setId(res.getInt("id"));
				task.setName(res.getString("name"));
				task.setStart_date(res.getDate("start_date"));
				task.setEnd_date(res.getDate("end_date"));
				task.setUser_id(res.getInt("user_id"));
				task.setJob_id(res.getInt("job_id"));
				task.setStatus_id(res.getInt("status_id"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return task;
	}

	public int addNewTask(Task task) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO tasks (name,start_date,end_date,user_id,job_id,status_id) VALUES (?,?,?,?,?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, task.getName());
			stm.setDate(2, task.getStart_date());
			stm.setDate(3, task.getEnd_date());
			stm.setInt(4, task.getUser_id());
			stm.setInt(5, task.getJob_id());
			stm.setInt(6, task.getStatus_id());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int editTask(Task task) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE tasks SET name = ?, start_date = ?, start_date = ?, user_id = ?, job_id = ?, status_id = ? WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, task.getName());
			stm.setDate(2, task.getStart_date());
			stm.setDate(3, task.getEnd_date());
			stm.setInt(4, task.getUser_id());
			stm.setInt(5, task.getJob_id());
			stm.setInt(6, task.getStatus_id());
			stm.setInt(7, task.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int editStatusId(Task task) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE tasks SET status_id = ? WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, task.getStatus_id());
			stm.setInt(2, task.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteTask(int id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM tasks WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<TasksByStatus> getListTasksByStatusOnJobDetailPage(int job_id) {
		List<TasksByStatus> listTasksByStatus = new LinkedList<TasksByStatus>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM status;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				TasksByStatus taskByStatus = new TasksByStatus();
				taskByStatus.setStatus_name(res.getString("name"));
				taskByStatus.setListTaskDTO(this.getListTaskDTOByJobAndStatus(job_id, res.getInt("id")));
				listTasksByStatus.add(taskByStatus);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listTasksByStatus;
	}

	public List<TasksByStatus> getListTasksByStatusOnUserDetailPage(int user_id) {
		List<TasksByStatus> listTasksByStatus = new LinkedList<TasksByStatus>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM status;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				TasksByStatus taskByStatus = new TasksByStatus();
				taskByStatus.setStatus_name(res.getString("name"));
				taskByStatus.setListTaskDTO(this.getListTaskDTOByUserAndStatus(user_id, res.getInt("id")));
				listTasksByStatus.add(taskByStatus);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listTasksByStatus;
	}

	public List<TaskDTO> getListTaskDTOByJobAndStatus(int job_id, int status_id) {
		List<TaskDTO> listTaskDTO = new LinkedList<TaskDTO>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT tasks.id as task_id, tasks.name as task_name, tasks.start_date, tasks.end_date,\n"
					+ "users.id as user_id, users.fullname as user_fullname, status.id as status_id ,status.name as status_name,\n"
					+ "jobs.name as job_name, jobs.id as job_id FROM tasks \n" + "JOIN jobs on tasks.job_id = jobs.id\n"
					+ "JOIN users on tasks.user_id = users.id\n"
					+ "JOIN status on tasks.status_id = status.id WHERE jobs.id = ? AND status.id = ? ";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, job_id);
			stm.setInt(2, status_id);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				TaskDTO taskDTO = new TaskDTO();
				taskDTO.setJob_id(res.getInt("job_id"));
				taskDTO.setJob_name(res.getString("job_name"));
				taskDTO.setTask_id(res.getInt("task_id"));
				taskDTO.setTask_name(res.getString("task_name"));
				taskDTO.setStart_date(res.getDate("start_date"));
				taskDTO.setEnd_date(res.getDate("end_date"));
				taskDTO.setStatus_name(res.getString("status_name"));
				taskDTO.setUser_id(res.getInt("user_id"));
				taskDTO.setUser_fullname(res.getString("user_fullname"));
				listTaskDTO.add(taskDTO);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listTaskDTO;
	}

	public List<TaskDTO> getListTaskDTOByUserAndStatus(int user_id, int status_id) {
		List<TaskDTO> listTaskDTO = new LinkedList<TaskDTO>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT tasks.id as task_id, tasks.name as task_name, tasks.start_date, tasks.end_date,\n"
					+ "users.id as user_id, users.fullname as user_fullname, status.id as status_id ,status.name as status_name,\n"
					+ "jobs.name as job_name, jobs.id as job_id FROM tasks \n" + "JOIN jobs on tasks.job_id = jobs.id\n"
					+ "JOIN users on tasks.user_id = users.id\n"
					+ "JOIN status on tasks.status_id = status.id WHERE users.id = ? AND status.id = ? ";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, user_id);
			stm.setInt(2, status_id);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				TaskDTO taskDTO = new TaskDTO();
				taskDTO.setJob_id(res.getInt("job_id"));
				taskDTO.setJob_name(res.getString("job_name"));
				taskDTO.setTask_id(res.getInt("task_id"));
				taskDTO.setTask_name(res.getString("task_name"));
				taskDTO.setStart_date(res.getDate("start_date"));
				taskDTO.setEnd_date(res.getDate("end_date"));
				taskDTO.setStatus_name(res.getString("status_name"));
				taskDTO.setUser_id(res.getInt("user_id"));
				taskDTO.setUser_fullname(res.getString("user_fullname"));
				listTaskDTO.add(taskDTO);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listTaskDTO;
	}

	public List<TaskDTO> getListTaskDTOByJobId(int job_id) {
		List<TaskDTO> listTaskDTO = new LinkedList<TaskDTO>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT tasks.id as task_id, tasks.name as task_name, tasks.start_date, tasks.end_date,\n"
					+ "users.id as user_id, users.fullname as user_fullname, status.id as status_id ,status.name as status_name,\n"
					+ "jobs.name as job_name, jobs.id as job_id FROM tasks \n" + "JOIN jobs on tasks.job_id = jobs.id\n"
					+ "JOIN users on tasks.user_id = users.id\n"
					+ "JOIN status on tasks.status_id = status.id WHERE jobs.id = ? ORDER BY tasks.start_date ASC;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, job_id);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				TaskDTO taskDTO = new TaskDTO();
				taskDTO.setJob_id(res.getInt("job_id"));
				taskDTO.setJob_name(res.getString("job_name"));
				taskDTO.setTask_id(res.getInt("task_id"));
				taskDTO.setTask_name(res.getString("task_name"));
				taskDTO.setStart_date(res.getDate("start_date"));
				taskDTO.setEnd_date(res.getDate("end_date"));
				taskDTO.setStatus_name(res.getString("status_name"));
				taskDTO.setUser_id(res.getInt("user_id"));
				taskDTO.setUser_fullname(res.getString("user_fullname"));
				listTaskDTO.add(taskDTO);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listTaskDTO;
	}

	public List<TaskDTO> getListTaskDTOByUserId(int user_id) {
		List<TaskDTO> listTaskDTO = new LinkedList<TaskDTO>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT tasks.id as task_id, tasks.name as task_name, tasks.start_date, tasks.end_date,\n"
					+ "users.id as user_id, users.fullname as user_fullname, status.id as status_id ,status.name as status_name,\n"
					+ "jobs.name as job_name, jobs.id as job_id FROM tasks \n" + "JOIN jobs on tasks.job_id = jobs.id\n"
					+ "JOIN users on tasks.user_id = users.id\n"
					+ "JOIN status on tasks.status_id = status.id WHERE users.id = ? ORDER BY tasks.start_date ASC;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, user_id);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				TaskDTO taskDTO = new TaskDTO();
				taskDTO.setJob_id(res.getInt("job_id"));
				taskDTO.setJob_name(res.getString("job_name"));
				taskDTO.setTask_id(res.getInt("task_id"));
				taskDTO.setTask_name(res.getString("task_name"));
				taskDTO.setStart_date(res.getDate("start_date"));
				taskDTO.setEnd_date(res.getDate("end_date"));
				taskDTO.setStatus_name(res.getString("status_name"));
				taskDTO.setUser_id(res.getInt("user_id"));
				taskDTO.setUser_fullname(res.getString("user_fullname"));
				listTaskDTO.add(taskDTO);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listTaskDTO;
	}

	public TaskDTO findTaskDTOByTaskId(int task_id) {
		TaskDTO taskDTO = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT tasks.id as task_id, tasks.name as task_name, tasks.start_date, tasks.end_date,\n"
					+ "users.id as user_id, users.fullname as user_fullname, status.id as status_id ,status.name as status_name,\n"
					+ "jobs.name as job_name, jobs.id as job_id FROM tasks \n" + "JOIN jobs on tasks.job_id = jobs.id\n"
					+ "JOIN users on tasks.user_id = users.id\n"
					+ "JOIN status on tasks.status_id = status.id WHERE tasks.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, task_id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				taskDTO = new TaskDTO();
				taskDTO.setJob_id(res.getInt("job_id"));
				taskDTO.setJob_name(res.getString("job_name"));
				taskDTO.setTask_id(res.getInt("task_id"));
				taskDTO.setTask_name(res.getString("task_name"));
				taskDTO.setStart_date(res.getDate("start_date"));
				taskDTO.setEnd_date(res.getDate("end_date"));
				taskDTO.setStatus_id(res.getInt("status_id"));
				taskDTO.setStatus_name(res.getString("status_name"));
				taskDTO.setUser_id(res.getInt("user_id"));
				taskDTO.setUser_fullname(res.getString("user_fullname"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return taskDTO;
	}
}
