package com.crmmvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.crmmvc.connection.JDBCConnection;
import com.crmmvc.dto.UserDTO;
import com.crmmvc.model.User;

public class UserDAO {
	public List<UserDTO> getUserDTOList() {
		List<UserDTO> listUserDTO = new LinkedList<UserDTO>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users JOIN roles ON users.role_id = roles.id;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				UserDTO userDTO = new UserDTO();
				userDTO.setId(res.getInt("id"));
				userDTO.setEmail(res.getString("email"));
				userDTO.setFullname(res.getString("fullname"));
				userDTO.setRole_name(res.getString("name"));
				listUserDTO.add(userDTO);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listUserDTO;
	}

	public List<User> getMemeberUserList() {
		List<User> listUser = new LinkedList<User>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users JOIN roles ON users.role_id = roles.id WHERE roles.name = 'ROLE_MEMBER';";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				User user = new User();
				user.setId(res.getInt("id"));
				user.setFullname(res.getString("fullname"));
				listUser.add(user);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listUser;
	}

	public int addNewUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO users (fullname,email,password,role_id) VALUES(?,?,?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, user.getFullname());
			stm.setString(2, user.getEmail());
			stm.setString(3, user.getPassword());
			stm.setInt(4, user.getRole_id());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteUser(int id_delete) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM users WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id_delete);
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public User findById(int id_find) {
		User user = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id_find);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				user = new User();
				user.setId(id_find);
				user.setEmail(res.getString("email"));
				user.setPassword(res.getString("password"));
				user.setFullname(res.getString("fullname"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	public UserDTO findUserDTOById(int id_find) {
		UserDTO userDTO = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT users.email, users.password, users.fullname, roles.id as role_id ,roles.name as role_name FROM users JOIN roles ON users.role_id = roles.id WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id_find);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				userDTO = new UserDTO();
				userDTO.setId(id_find);
				userDTO.setEmail(res.getString("email"));
				userDTO.setPassword(res.getString("password"));
				userDTO.setFullname(res.getString("fullname"));
				userDTO.setRole_id(res.getInt("role_id"));
				userDTO.setRole_name(res.getString("role_name"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDTO;
	}

	public int editInfUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE users SET fullname = ?, role_id = ? WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, user.getFullname());
			stm.setInt(2, user.getRole_id());
			stm.setInt(3, user.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int editPassUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE users SET password = ? WHERE id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, user.getPassword());
			stm.setInt(2, user.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public UserDTO findUserDTOByEmail(String email) {
		UserDTO userDTO = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users JOIN roles ON users.role_id = roles.id WHERE users.email = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, email);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				userDTO = new UserDTO();
				userDTO.setId(res.getInt("id"));
				userDTO.setEmail(res.getString("email"));
				userDTO.setPassword(res.getString("password"));
				userDTO.setFullname(res.getString("fullname"));
				userDTO.setRole_name(res.getString("name"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDTO;
	}

	public int[] getPercentageOfWords(int user_id) {
		int[] array = new int[4];
		final int NUMBER_OF_STATUS = 3;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT count(tasks.id) as numberOfTasks FROM tasks JOIN users "
					+ "ON tasks.user_id = users.id WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, user_id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				array[0] = res.getInt("numberOfTasks");
			}
			for (int i = 1; i <= NUMBER_OF_STATUS; i++) {
				array[i] = getNumberOfTask(user_id, i);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}

	private int getNumberOfTask(int user_id, int status_id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT count(tasks.id) as numberOfTasks FROM tasks JOIN users ON tasks.user_id = users.id"
					+ " JOIN status on tasks.status_id = status.id" + " WHERE users.id = ? AND status.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, user_id);
			stm.setInt(2, status_id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				result = res.getInt("numberOfTasks");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
