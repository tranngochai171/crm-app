package com.crmmvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.crmmvc.connection.JDBCConnection;
import com.crmmvc.model.Status;

public class StatusDAO {
	public List<Status> getListStatus() {
		List<Status> listStatus = new LinkedList<Status>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM status;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Status sta = new Status();
				sta.setId(res.getInt("id"));
				sta.setName(res.getString("name"));
				listStatus.add(sta);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listStatus;
	}
}
