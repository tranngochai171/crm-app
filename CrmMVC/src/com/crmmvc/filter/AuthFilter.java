package com.crmmvc.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.crmmvc.dto.UserDTO;
import com.crmmvc.util.UrlConstants;

@WebFilter(urlPatterns = "/*")
public class AuthFilter implements Filter {
	private final String LEADER = "ROLE_LEADER";
	private final String MEMBER = "ROLE_MEMBER";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String action = req.getServletPath();
		if (!action.startsWith(UrlConstants.URL_LOGIN)) {
			HttpSession session = req.getSession();
			if (session.getAttribute("USER") == null) {
				resp.sendRedirect(req.getContextPath() + UrlConstants.URL_LOGIN);
				return;
			}
			UserDTO user = (UserDTO) session.getAttribute("USER");
			if (user.getRole_name().equals(this.LEADER)) {
				if (action.startsWith(UrlConstants.URL_ROLE) || action.startsWith(UrlConstants.URL_USER)) {
					resp.sendRedirect(req.getContextPath() + UrlConstants.URL_403);
					return;
				}

			}
			if (user.getRole_name().equals(this.MEMBER)) {
				if (action.startsWith(UrlConstants.URL_USER_DETAIL)) {
					chain.doFilter(request, response);
					return;
				} else if (action.startsWith(UrlConstants.URL_ROLE) || action.startsWith(UrlConstants.URL_USER)
						|| action.startsWith(UrlConstants.URL_JOB)) {
					resp.sendRedirect(req.getContextPath() + UrlConstants.URL_403);
					return;
				}
			}
		}
		chain.doFilter(request, response);
		response.setCharacterEncoding("UTF-8");
	}

}
