package com.crmmvc.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import com.crmmvc.dao.UserDAO;
import com.crmmvc.dto.UserDTO;
import com.crmmvc.util.PathConstants;
import com.crmmvc.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_LOGIN })
public class LoginController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO;

	public LoginController() {
		this.userDAO = new UserDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getParameter("action") != null && req.getParameter("action").equals("logout")) {
			req.getSession().removeAttribute("USER");
		}
		req.getRequestDispatcher(PathConstants.PATH_LOGIN).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		UserDTO userDTO = this.userDAO.findUserDTOByEmail(email);
		if (userDTO == null || !BCrypt.checkpw(password, userDTO.getPassword())) {
			req.setAttribute("message", "Thông tin đăng nhập chưa chính xác");
			req.getRequestDispatcher(PathConstants.PATH_LOGIN).forward(req, resp);
			return;
		}
		req.getSession().setAttribute("USER", userDTO);
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_HOME);
	}
}
