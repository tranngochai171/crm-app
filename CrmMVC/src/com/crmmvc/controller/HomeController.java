package com.crmmvc.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crmmvc.util.CommonMethods;
import com.crmmvc.util.PathConstants;
import com.crmmvc.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_HOME })
public class HomeController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CommonMethods.notifyPopup(req, "fail");
		CommonMethods.notifyPopup(req, "success");
		req.getRequestDispatcher(PathConstants.PATH_HOME).forward(req, resp);
	}
}
