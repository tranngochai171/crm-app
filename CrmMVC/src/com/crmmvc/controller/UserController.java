package com.crmmvc.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import com.crmmvc.dao.RoleDAO;
import com.crmmvc.dao.TaskDAO;
import com.crmmvc.dao.UserDAO;
import com.crmmvc.dto.UserDTO;
import com.crmmvc.model.PercentageTasks;
import com.crmmvc.model.TasksByStatus;
import com.crmmvc.model.User;
import com.crmmvc.util.CommonMethods;
import com.crmmvc.util.PathConstants;
import com.crmmvc.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_USER, UrlConstants.URL_USER_ADD, UrlConstants.URL_USER_DELETE,
		UrlConstants.URL_USER_EDIT, UrlConstants.URL_USER_EDIT_PASS, UrlConstants.URL_USER_DETAIL })
public class UserController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO;
	private RoleDAO roleDAO;
	private TaskDAO taskDAO;

	public UserController() {
		this.userDAO = new UserDAO();
		this.roleDAO = new RoleDAO();
		this.taskDAO = new TaskDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		CommonMethods.notifyPopup(req, "success");
		CommonMethods.notifyPopup(req, "fail");
		switch (action) {
		case UrlConstants.URL_USER_ADD:
			req.setAttribute("listRole", this.roleDAO.getListRole());
			req.getRequestDispatcher(PathConstants.PATH_USER_ADD).forward(req, resp);
			return;
		case UrlConstants.URL_USER_EDIT:
			req.setAttribute("listRole", this.roleDAO.getListRole());
			this.redirectEditPageUser(req, resp, PathConstants.PATH_USER_EDIT);
			break;
		case UrlConstants.URL_USER_EDIT_PASS:
			this.redirectEditPageUser(req, resp, PathConstants.PATH_USER_EDIT_PASS);
			break;
		case UrlConstants.URL_USER_DETAIL:
			this.redirectEditPageUserDTO(req, resp, PathConstants.PATH_USER_DETAIL);
			break;
		case UrlConstants.URL_USER_DELETE:
			if (req.getParameter("id") != null) {
				int id_delete = Integer.parseInt(req.getParameter("id"));
				if (this.userDAO.deleteUser(id_delete) <= 0)
					req.setAttribute("fail", "Xoá thành viên thất bại");
				else
					req.setAttribute("success", "Xóa thành viên thành công");
			}
			break;
		}
		List<UserDTO> listUser = this.userDAO.getUserDTOList();
		req.setAttribute("listUser", listUser);
		req.getRequestDispatcher(PathConstants.PATH_USER).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		String fullname = "";
		int role_id = 0, id;
		String email = req.getParameter("email");
		if (!action.equals(UrlConstants.URL_USER_EDIT_PASS)) {
			fullname = req.getParameter("fullname");
			role_id = Integer.parseInt(req.getParameter("role_id"));
		}
		switch (action) {
		case UrlConstants.URL_USER_ADD:
			String password = BCrypt.hashpw(req.getParameter("password"), BCrypt.gensalt(12));
			User user_add = new User(email, password, fullname, role_id);
			if (this.userDAO.addNewUser(user_add) <= 0)
				req.getSession().setAttribute("fail", "Thêm mới thành viên thất bại");
			else
				req.getSession().setAttribute("success", "Thêm mới thành viên thành công");
			break;
		case UrlConstants.URL_USER_EDIT: {
			id = Integer.parseInt(req.getParameter("id"));
			User user_edit = new User(id, fullname, role_id);
			if (this.userDAO.editInfUser(user_edit) <= 0)
				req.getSession().setAttribute("fail", "Sửa thành viên thất bại");
			else
				req.getSession().setAttribute("success", "Sửa thành viên thành công");
		}
			break;
		case UrlConstants.URL_USER_EDIT_PASS: {
			id = Integer.parseInt(req.getParameter("id"));
			User user_check = this.checkPasswordAndReturnUser(req, id);
			if (user_check == null) {
				req.setAttribute("item", this.userDAO.findById(id));
				req.getRequestDispatcher(PathConstants.PATH_USER_EDIT_PASS).forward(req, resp);
				return;
			}
			if (this.userDAO.editPassUser(user_check) <= 0)
				req.getSession().setAttribute("fail", "Cập nhật mật khẩu thất bại");
			else
				req.getSession().setAttribute("success", "Cập nhật mật khẩu thành công");
		}
			break;
		}
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_USER);
	}

	private void redirectEditPageUser(HttpServletRequest req, HttpServletResponse resp, String path)
			throws ServletException, IOException {
		if (req.getParameter("id") != null) {
			int id_find = Integer.parseInt(req.getParameter("id"));
			UserDTO userDTO_edit = this.userDAO.findUserDTOById(id_find);
			if (userDTO_edit != null) {
				req.setAttribute("item", userDTO_edit);
				req.getRequestDispatcher(path).forward(req, resp);
				return;
			}
			req.setAttribute("fail", "Không tìm thấy thành viên cần chỉnh sửa");
		}
	}

	private void redirectEditPageUserDTO(HttpServletRequest req, HttpServletResponse resp, String pathEdit)
			throws ServletException, IOException {
		if (req.getParameter("id") != null) {
			int id_find = Integer.parseInt(req.getParameter("id"));
			UserDTO userDTO = this.userDAO.findUserDTOById(id_find);
			if (userDTO != null) {
				List<TasksByStatus> listTasksByStatus = this.taskDAO
						.getListTasksByStatusOnUserDetailPage(userDTO.getId());
				int[] array = this.userDAO.getPercentageOfWords(userDTO.getId());
				PercentageTasks pt = new PercentageTasks(array[0], array[1], array[2], array[3]);
				req.setAttribute("pt", pt);
				req.setAttribute("listTasksByStatus", listTasksByStatus);
				req.setAttribute("item", userDTO);
				req.getRequestDispatcher(pathEdit).forward(req, resp);
				return;
			}
			req.setAttribute("fail", "Không tìm thấy thành viên");
		}
	}

	private User checkPasswordAndReturnUser(HttpServletRequest req, int id_check) {
		String oldPassword = req.getParameter("oldpassword");
		String newPassword = req.getParameter("newpassword");
		User user_check = this.userDAO.findById(id_check);
		if (!BCrypt.checkpw(oldPassword, user_check.getPassword())) {
			req.setAttribute("message", "Mật khẩu cũ chưa chính xác");
			return null;
		}
		if (oldPassword.equals(newPassword)) {
			req.setAttribute("message", "Mật khẩu cũ trùng với mật khẩu mới");
			return null;
		}
		user_check.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt(12)));
		return user_check;
	}
}
