package com.crmmvc.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crmmvc.dao.JobDAO;
import com.crmmvc.dao.TaskDAO;
import com.crmmvc.model.Job;
import com.crmmvc.model.PercentageTasks;
import com.crmmvc.model.TasksByStatus;
import com.crmmvc.util.CommonMethods;
import com.crmmvc.util.PathConstants;
import com.crmmvc.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_JOB, UrlConstants.URL_JOB_ADD, UrlConstants.URL_JOB_DELETE,
		UrlConstants.URL_JOB_EDIT, UrlConstants.URL_JOB_DETAIL })
public class JobController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JobDAO jobDAO;
	private TaskDAO taskDAO;

	public JobController() {
		this.jobDAO = new JobDAO();
		this.taskDAO = new TaskDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		CommonMethods.notifyPopup(req, "success");
		CommonMethods.notifyPopup(req, "fail");
		switch (action) {
		case UrlConstants.URL_JOB_ADD:
			req.getRequestDispatcher(PathConstants.PATH_JOB_ADD).forward(req, resp);
			return;
		case UrlConstants.URL_JOB_DETAIL:
			if (req.getParameter("id") != null) {
				Job job = this.jobDAO.findById(Integer.parseInt(req.getParameter("id")));
				if (job != null) {
					List<TasksByStatus> listTasksByStatus = this.taskDAO
							.getListTasksByStatusOnJobDetailPage(job.getId());
					int[] array = this.jobDAO.getPercentageOfWords(job.getId());
					PercentageTasks pt = new PercentageTasks(array[0], array[1], array[2], array[3]);
					req.setAttribute("pt", pt);
					req.setAttribute("listTasksByStatus", listTasksByStatus);
					req.setAttribute("item", job);
					req.getRequestDispatcher(PathConstants.PATH_JOB_DETAIL).forward(req, resp);
					return;
				}
				req.setAttribute("fail", "Không tìm thấy công việc");
			}
			break;
		case UrlConstants.URL_JOB_EDIT:
			if (req.getParameter("id") != null) {
				Job job = this.jobDAO.findById(Integer.parseInt(req.getParameter("id")));
				if (job != null) {
					req.setAttribute("item", job);
					req.getRequestDispatcher(PathConstants.PATH_JOB_EDIT).forward(req, resp);
					return;
				}
			}
			req.setAttribute("fail", "Không tìm thấy công việc");
			break;
		case UrlConstants.URL_JOB_DELETE:
			if (req.getParameter("id") != null) {
				if (this.jobDAO.deleteJob(Integer.parseInt(req.getParameter("id"))) <= 0)
					req.setAttribute("fail", "Xóa công việc thất bại");
				else
					req.setAttribute("success", "Xóa công việc thành công");
			}
			break;
		}
		req.setAttribute("listJob", this.jobDAO.getListJob());
		req.getRequestDispatcher(PathConstants.PATH_JOB).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		String name = req.getParameter("name");
		Date start_date = Date.valueOf(req.getParameter("start_date"));
		Date end_date = Date.valueOf(req.getParameter("end_date"));
		switch (action) {
		case UrlConstants.URL_JOB_ADD:
			if (start_date.after(end_date)) {
				req.setAttribute("message", "Ngày bắt đầu và ngày kết thúc không hợp lệ");
				req.getRequestDispatcher(PathConstants.PATH_JOB_ADD).forward(req, resp);
				return;
			}
			Job job_add = new Job(name, start_date, end_date);
			if (this.jobDAO.addNewJob(job_add) <= 0)
				req.getSession().setAttribute("fail", "Thêm mới công việc thất bại");
			else
				req.getSession().setAttribute("success", "Thêm mới công việc thành công");
			break;
		case UrlConstants.URL_JOB_EDIT:
			int id = Integer.parseInt(req.getParameter("id"));
			Job job_edit = new Job(id, name, start_date, end_date);
			if (start_date.after(end_date)) {
				req.setAttribute("item", job_edit);
				req.setAttribute("message", "Ngày bắt đầu và ngày kết thúc không hợp lệ");
				req.getRequestDispatcher(PathConstants.PATH_JOB_ADD).forward(req, resp);
				return;
			}
			if (this.jobDAO.editJob(job_edit) <= 0)
				req.getSession().setAttribute("fail", "Cập nhật công việc thất bại");
			else
				req.getSession().setAttribute("success", "Cập nhật công việc thành công");
			break;
		}
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_JOB);
	}
}
