package com.crmmvc.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crmmvc.dao.JobDAO;
import com.crmmvc.dao.StatusDAO;
import com.crmmvc.dao.TaskDAO;
import com.crmmvc.dao.UserDAO;
import com.crmmvc.dto.TaskDTO;
import com.crmmvc.model.Job;
import com.crmmvc.model.Task;
import com.crmmvc.model.User;
import com.crmmvc.util.PathConstants;
import com.crmmvc.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_TASK_ADD, UrlConstants.URL_TASK_DETAIL })
public class TaskController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TaskDAO taskDAO;
	private StatusDAO statusDAO;
	private UserDAO userDAO;
	private JobDAO jobDAO;

	public TaskController() {
		this.taskDAO = new TaskDAO();
		this.statusDAO = new StatusDAO();
		this.userDAO = new UserDAO();
		this.jobDAO = new JobDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		switch (action) {
		case UrlConstants.URL_TASK_ADD:
			if (req.getParameter("job_id") != null) {
				Job job = this.jobDAO.findById(Integer.parseInt(req.getParameter("job_id")));
				if (job == null) {
					req.getSession().setAttribute("fail", "Không tìm thấy công việc");
					resp.sendRedirect(UrlConstants.URL_JOB);
					return;
				}
				List<User> listMemberUser = this.userDAO.getMemeberUserList();
				if (listMemberUser.isEmpty()) {
					req.setAttribute("item", job);
					req.setAttribute("fail", "Danh sách thành viên đang trống không thể thêm task");
					req.getRequestDispatcher(PathConstants.PATH_JOB_DETAIL).forward(req, resp);
					return;
				}
				req.setAttribute("job", job);
				req.setAttribute("listUser", listMemberUser);
				req.setAttribute("listStatus", this.statusDAO.getListStatus());
				req.getRequestDispatcher(PathConstants.PATH_TASK_ADD).forward(req, resp);
				return;
			}
		case UrlConstants.URL_TASK_DETAIL:
			if (req.getParameter("id") != null) {
				int task_id = Integer.parseInt(req.getParameter("id"));
				TaskDTO taskDTO = this.taskDAO.findTaskDTOByTaskId(task_id);
				if (taskDTO != null) {
					req.setAttribute("listStatus", this.statusDAO.getListStatus());
					req.setAttribute("item", taskDTO);
					req.getRequestDispatcher(PathConstants.PATH_TASK_DETAIL).forward(req, resp);
					return;
				}
			}
			req.getSession().setAttribute("fail", "Không tìm thấy task");
			resp.sendRedirect(req.getContextPath() + UrlConstants.URL_HOME);
			return;
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		switch (action) {
		case UrlConstants.URL_TASK_DETAIL:
			int task_id = Integer.parseInt(req.getParameter("task_id"));
			int job_id = Integer.parseInt(req.getParameter("job_id"));
			int status_id = Integer.parseInt(req.getParameter("status_id"));
			Task task_edit = new Task(task_id, status_id);
			if (this.taskDAO.editStatusId(task_edit) <= 0)
				req.getSession().setAttribute("fail", "Cập nhật task thất bại");
			else
				req.getSession().setAttribute("success", "Cập nhật task thành công");
			resp.sendRedirect(req.getContextPath() + UrlConstants.URL_JOB_DETAIL + "?id=" + job_id);
			return;
		}
		int job_id = Integer.parseInt(req.getParameter("job_id"));
		int user_id = Integer.parseInt(req.getParameter("user_id"));
		int status_id = Integer.parseInt(req.getParameter("status_id"));
		String name = req.getParameter("name");
		Date start_date = Date.valueOf(req.getParameter("start_date"));
		Date end_date = Date.valueOf(req.getParameter("end_date"));
		Job job_detail = this.jobDAO.findById(job_id);
		if (start_date.after(end_date)) {
			req.setAttribute("message", "Ngày bắt đầu và ngày kết thúc không hợp lệ");
			req.setAttribute("job", job_detail);
			req.setAttribute("listUser", this.userDAO.getMemeberUserList());
			req.setAttribute("listStatus", this.statusDAO.getListStatus());
			req.getRequestDispatcher(PathConstants.PATH_TASK_ADD).forward(req, resp);
			return;
		} else {
			Task task_add = new Task(name, start_date, end_date, user_id, job_id, status_id);
			if (this.taskDAO.addNewTask(task_add) <= 0)
				req.getSession().setAttribute("fail", "Thêm mới task thất bại");
			else
				req.getSession().setAttribute("success", "Thêm mới task thành công");
		}
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_JOB_DETAIL + "?id=" + job_detail.getId());
	}
}
