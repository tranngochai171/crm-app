package com.crmmvc.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crmmvc.dao.RoleDAO;
import com.crmmvc.model.Role;
import com.crmmvc.util.CommonMethods;
import com.crmmvc.util.PathConstants;
import com.crmmvc.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_ROLE, UrlConstants.URL_ROLE_ADD, UrlConstants.URL_ROLE_DELETE,
		UrlConstants.URL_ROLE_EDIT })
public class RoleController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RoleDAO roleDAO;

	public RoleController() {
		this.roleDAO = new RoleDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		CommonMethods.notifyPopup(req, "success");
		CommonMethods.notifyPopup(req, "fail");
		switch (action) {
		case UrlConstants.URL_ROLE_ADD:
			req.getRequestDispatcher(PathConstants.PATH_ROLE_ADD).forward(req, resp);
			return;
		case UrlConstants.URL_ROLE_EDIT:
			if (req.getParameter("id") != null) {
				int id = Integer.parseInt(req.getParameter("id"));
				Role role = this.roleDAO.findById(id);
				if (role != null) {
					req.setAttribute("item", role);
					req.getRequestDispatcher(PathConstants.PATH_ROLE_EDIT).forward(req, resp);
					return;
				}
				req.setAttribute("fail", "Không tìm thấy quyền cần chỉnh sửa");
			}
			break;
		case UrlConstants.URL_ROLE_DELETE:
			if (req.getParameter("id") != null) {
				int id_delete = Integer.parseInt(req.getParameter("id"));
				if (this.roleDAO.deleteRole(id_delete) <= 0)
					req.setAttribute("fail", "Xóa quyền thất bại");
				else
					req.setAttribute("success", "Xóa quyền thành công");
			}
			break;
		}
		List<Role> listRole = this.roleDAO.getListRole();
		req.setAttribute("listRole", listRole);

		req.getRequestDispatcher(PathConstants.PATH_ROLE).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		String name = req.getParameter("name");
		String description = req.getParameter("description");
		switch (action) {
		case UrlConstants.URL_ROLE_ADD:
			Role role_add = new Role(name, description);
			if (this.roleDAO.addNewRole(role_add) <= 0)
				req.getSession().setAttribute("fail", "Thêm mới quyền thất bại");
			else
				req.getSession().setAttribute("success", "Thêm mới quyền thành công");
			break;
		case UrlConstants.URL_ROLE_EDIT:
			int id = Integer.parseInt(req.getParameter("id"));
			Role role_edit = new Role(id, name, description);
			if (this.roleDAO.editRole(role_edit) <= 0)
				req.getSession().setAttribute("fail", "Sửa quyền thất bại");
			else
				req.getSession().setAttribute("success", "Sửa quyền thành công");
			break;
		}
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_ROLE);
	}
}
