package com.crmmvc.model;

public class PercentageTasks {
	private String notDone;
	private String inProgress;
	private String done;

	public String getNotDone() {
		return notDone;
	}

	public void setNotDone(String notDone) {
		this.notDone = notDone;
	}

	public String getInProgress() {
		return inProgress;
	}

	public void setInProgress(String inProgress) {
		this.inProgress = inProgress;
	}

	public String getDone() {
		return done;
	}

	public void setDone(String done) {
		this.done = done;
	}

	public PercentageTasks(int allTasks, int notDone, int inProgress, int done) {
		this.notDone = String.format("%.0f", (float) (notDone * 1.0 / allTasks) * 100);
		this.inProgress = String.format("%.0f", (float) (inProgress * 1.0 / allTasks) * 100);
		this.done = String.format("%.0f", (float) (done * 1.0 / allTasks) * 100);
	}

}
