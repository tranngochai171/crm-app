package com.crmmvc.model;

import java.util.LinkedList;
import java.util.List;

import com.crmmvc.dto.TaskDTO;

public class TasksByStatus {
	private String status_name;
	private List<TaskDTO> listTaskDTO;

	public String getStatus_name() {
		return status_name;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public List<TaskDTO> getListTaskDTO() {
		return listTaskDTO;
	}

	public void setListTaskDTO(List<TaskDTO> listTaskDTO) {
		this.listTaskDTO = listTaskDTO;
	}

	public TasksByStatus() {
		this.listTaskDTO = new LinkedList<TaskDTO>();
	}
}
