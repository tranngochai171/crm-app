package com.crmmvc.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CommonMethods {

	public static void notifyPopup(HttpServletRequest req, String notification) {
		HttpSession session = req.getSession();
		if (session != null && session.getAttribute(notification) != null) {
			String message = (String) session.getAttribute(notification);
			req.setAttribute(notification, message);
			session.removeAttribute(notification);
		}
	}
}
