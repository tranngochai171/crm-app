package com.crmmvc.util;

public class PathConstants {
	// LOGIN VIEW
	public static final String PATH_LOGIN = "/views/login/index.jsp";
	// HOME VIEW
	public static final String PATH_HOME = "/views/home/index.jsp";
	// ERROR VIEW
	public static final String PATH_403 = "/views/errors/403.jsp";
	public static final String PATH_404 = "/views/errors/404.jsp";
	// ROLE VIEW
	public static final String PATH_ROLE = "/views/role/index.jsp";
	public static final String PATH_ROLE_ADD = "/views/role/add.jsp";
	public static final String PATH_ROLE_EDIT = "/views/role/edit.jsp";
	// USER VIEW
	public static final String PATH_USER = "/views/user/index.jsp";
	public static final String PATH_USER_ADD = "/views/user/add.jsp";
	public static final String PATH_USER_EDIT = "/views/user/edit.jsp";
	public static final String PATH_USER_EDIT_PASS = "/views/user/edit-pass.jsp";
	public static final String PATH_USER_DETAIL = "/views/user/detail.jsp";
	// TASK VIEW
	public static final String PATH_TASK_ADD = "/views/task/add.jsp";
	public static final String PATH_TASK_DETAIL = "/views/task/detail.jsp";
	// JOB VIEW
	public static final String PATH_JOB = "/views/job/index.jsp";
	public static final String PATH_JOB_ADD = "/views/job/add.jsp";
	public static final String PATH_JOB_EDIT = "/views/job/edit.jsp";
	public static final String PATH_JOB_DETAIL = "/views/job/detail.jsp";
	// LAYOUT VIEW
	public static final String PATH_STYLE = "/views/layouts/style.jsp";
	public static final String PATH_SCRIPT = "/views/layouts/script.jsp";
	public static final String PATH_NAVBAR = "/views/layouts/navbar.jsp";
	public static final String PATH_SIDEBAR = "/views/layouts/sidebar.jsp";
	public static final String PATH_FOOTER = "/views/layouts/footer.jsp";
}
