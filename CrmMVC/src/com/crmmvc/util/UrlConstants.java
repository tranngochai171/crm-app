package com.crmmvc.util;

public class UrlConstants {
	// HOME CONTROLLER
	public static final String URL_HOME = "/index";
	// HOME CONTROLLER
	public static final String URL_LOGIN = "/login";
	// USER CONTROLLER
	public static final String URL_USER = "/user";
	public static final String URL_USER_ADD = "/user-add";
	public static final String URL_USER_DELETE = "/user-delete";
	public static final String URL_USER_EDIT = "/user-edit";
	public static final String URL_USER_EDIT_PASS = "/user-edit-pass";
	public static final String URL_USER_DETAIL = "/user-detail";
	// ROLE CONTROLLER
	public static final String URL_ROLE = "/role";
	public static final String URL_ROLE_ADD = "/role-add";
	public static final String URL_ROLE_DELETE = "/role-delete";
	public static final String URL_ROLE_EDIT = "/role-edit";
	// TASK CONTROLLER
	public static final String URL_TASK_ADD = "/task-add";
	public static final String URL_TASK_DETAIL = "/task-detail";
	// JOB CONTROLLER
	public static final String URL_JOB = "/job";
	public static final String URL_JOB_ADD = "/job-add";
	public static final String URL_JOB_DELETE = "/job-delete";
	public static final String URL_JOB_EDIT = "/job-edit";
	public static final String URL_JOB_DETAIL = "/job-detail";
	// ERROR CONTROLLER
	public static final String URL_403 = "/403";
	public static final String URL_404 = "/404";
}
