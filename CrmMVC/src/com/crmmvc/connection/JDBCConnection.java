package com.crmmvc.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
	final static String DATABASE = "jdbc:mysql://localhost:3306/crm";
	final static String USERNAME = "root";
	final static String PASSWORD = "lemonat96";

	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			return DriverManager.getConnection(DATABASE, USERNAME, PASSWORD);
		} catch (ClassNotFoundException e) {
			System.out.println("Không thể kết nối đến Driver");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Không thể kết nối đến Database");
			e.printStackTrace();
		}
		return null;
	}
}
